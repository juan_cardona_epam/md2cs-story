---
repository: 
---

# Command Lambda Stream Story

**Estimated reading time**: 5 minutes

## Story Outline

A story about a story.

## Story Organization
**Story Branch**: main

> `git checkout main`

**Practical task tag for self-study**: task

> `git checkout task`

Tags: #command_design_pattern, #generics, #lambda_functions, #streams

---
focus:
---

### Page 1

Page 1

---
focus:
---

### Page 2

Page 2

---
focus:
---

### Page 3

Page 3

---
focus:
---

### Page 4

Page 4

---
focus:
---

### Conclusions

* We have shown a way of like a fully oriented world to objects we can
  use the characteristics that exist in the functional languages such
  as lambdas expressions.
* Generality not only works in the early stages of object-oriented
  programming but is also the fundamental basis of programming with
  lambdas expressions.
* A language like Java is a language because of a large number of
  builders.
